﻿$(document).ready(function () {
    SendContact();
    ResetContact();
    //ContactFieldBlur();
    //GoogleMap(".contact_map");
    TextFeildFocus(".contact_txt, .contact_txta");
});
function TextFeildFocus(control) {
    $(control).focus(function () {
        $(this).parent().find("span").hide();
    });
    $(control).blur(function () {
        var txt_bg = $(this).parent();
        var mes = $(txt_bg).find("label");
        if ($(this).val() == txt_bg.find("span").text() || $(this).val() == "") {
            txt_bg.find("span").show();
            ContactFormMessage();
            var invalid_field = name_e;
            if ($(this).attr("id") == 'txt_contact_email') {
                invalid_field = email_e;
            } else if ($(this).attr("id") == 'txt_contact_address') {
                invalid_field = address_e;
            } else if ($(this).attr("id") == 'txt_contact_phone') {
                invalid_field = phone_e;
            } else if ($(this).attr("id") == 'txt_contact_content') {
                invalid_field = content_e;
            }
            $(this).val("");
            $(mes).html(invalid_field);
        } else {
            if ($(this).attr("id") == 'txt_contact_email' && regEmail.test($('#txt_contact_email').val()) == false) {
                $(mes).html(email_wrong_e);
            } else {
                $(this).parent().find("span").hide();
                $(mes).html(valid_field);
            }
        }
    });
}
var check = true;
var regEmail = /\w+\@\w+\.\w/;
//var checkEmail = regEmail.test($('#txt_contact_email').val());
var valid_field = "<span class='valid_icon'></span>";
var valid_content_field = "<span class='valid_content_icon'></span>";
var name_e = "<span class='invalid_icon'></span><span class='invalid_text'>Vui lòng nhập họ và tên</span>";
var email_e = "<span class='invalid_icon'></span><span class='invalid_text'>Vui lòng nhập địa chỉ email</span>";
var email_wrong_e = "<span class='invalid_icon'></span><span class='invalid_text'>Địa chỉ email không hợp lệ</span>";
var phone_e = "<span class='invalid_icon'></span><span class='invalid_text'>Vui lòng nhập số điện thoại</span>";
var address_e = "<span class='invalid_icon'></span><span class='invalid_text'>Vui lòng nhập địa chỉ</span>";
var content_e = "<span class='invalid_icon'></span><span class='invalid_text'>Vui lòng nhập nội dung liên hệ</span>";
function ContactFormMessage() {
    check = true;
    if ($.cookie("HGTLanguage") == "en") {       
        name_e = "<span class='invalid_icon'></span><span class='invalid_text'>Please enter your name</span>";
        email_e = "<span class='invalid_icon'></span><span class='invalid_text'>Please enter email address</span>";
        email_wrong_e = "<span class='invalid_icon'></span><span class='invalid_text'>Wrong type email</span>";
        phone_e = "<span class='invalid_icon'></span><span class='invalid_text'>Please enter phone number</span>";
        address_e = "<span class='invalid_icon'></span><span class='invalid_text'>Please enter address</span>";
        content_e = "<span class='invalid_icon'></span><span class='invalid_text'>Please enter your message</span>";
    } /*else if ($.cookie("DXLanguage") == "ru") {
        name_e = "<span class='invalid_icon'></span><span class='invalid_text'>введите имя.</span>";
        email_e = "<span class='invalid_icon'></span><span class='invalid_text'>Ваш адрес электронной.</span>";
        email_wrong_e = "<span class='invalid_icon'></span><span class='invalid_text'>Неверный адрес почты.</span>";
        address_e = "<span class='invalid_icon'></span><span class='invalid_text'>введите адрес.</span>";
        phone_e = "<span class='invalid_icon'></span><span class='invalid_text'>введите номер телефона.</span>";
        content_e = "<span class='invalid_content_icon'></span><span class='invalid_content_text'>Введите содержимое сообщения.</span>";

    }*/
}
function ContactValidation() {
    ContactFormMessage();
    if ($('#txt_contact_name').val() == "") {
        $('#name_error_mes').html(name_e);
        check = false;
    } else $('#name_error_mes').html(valid_field);
    if ($('#txt_contact_email').val() == "") {
        $('#email_error_mes').html(email_e);
        check = false;
    } else if (regEmail.test($('#txt_contact_email').val()) == false) {
        $('#email_error_mes').html(email_wrong_e);
        check = false;
    } else $('#email_error_mes').html(valid_field);
    if ($('#txt_contact_address').val() == "") {
        $('#address_error_mes').html(address_e);
        check = false;
    } else $('#address_error_mes').html(valid_field);
    if ($('#txt_contact_phone').val() == "") {
        $('#phone_error_mes').html(phone_e);
        check = false;
    } else $('#phone_error_mes').html(valid_field);
    if ($('#txt_contact_content').val() == "") {
        $('#content_error_mes').html(content_e);
        check = false;
    } else $('#content_error_mes').html(valid_content_field);
    return check;
}
function ResetContact() {
    $('.contact_button_reset').click(function () {
        $(".contact_txt, .contact_txta").val("");
        $("label").html("");
        $('.contact_txt_bg span, .contact_txta_bg span').show();
        return false;
    });
}
//-----------------------------------------------Send Contact----------------------------------------------//
function SendContact() {
    DialogMessage();
    $('.contact_button').click(function () {
        if (ContactValidation()) {
            $('.loading').fadeIn();
            var name = $('#txt_contact_name').val();
            var email = $('#txt_contact_email').val();
            var address = $('#txt_contact_address').val();
            var title = $('#txt_contact_phone').val();
            var content = $('#txt_contact_content').val();            
            var message = "Chúng tôi đã nhận được thông tin liên hệ của bạn và sẽ liên lạc lại trong gian sớm nhất.<br />Trân trọng cảm ơn !";
            var dialog_title = "Nội dung đã được gởi đi";
            if ($.cookie("HGTLanguage") == "vi") {
                message = "We have received your information contact and will be in contact shortly.<br />Thanks and best regards !";
                dialog_title = "Contact has been sent";
            }
            /*else if ($.cookie("DXLanguage") == "ru") {
            message = "Мы получили вашу контактную информацию, и воля связаться в ближайшее время.<br />Спасибо !";
            dialog_title = "Сообщение было отправлено";
            }*/
            //alert(client_info);
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: ResolveUrl("~/") + "Page/Contact.aspx/SendContact",
                data: "{name:'" + name + "', email:'" + email + "', address:'" + address + "', title:'" + title + "', content:'" + content + "'}",
                dataType: "json",
                success: function (data) {
                    //response(data);                    
                    $("#dialog-message .message").html(message);
                    $("#dialog-message").dialog('option', 'title', dialog_title);
                    //$("#dialog-message").dialog("option", "position", "center");
                    $("#dialog-message").dialog("open");
                    $('.loading').fadeOut();
                },
                error: function (result) {
                    alert('Error');
                }
            });
        } else return false;
    });
}