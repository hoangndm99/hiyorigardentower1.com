
google.maps.event.addDomListener(window, 'load', loadMap);

function loadMap() {
	// Basic options for a simple Google Map
	// For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
    var myLatlng = new google.maps.LatLng(15.917943, 108.330221);
	var image = ResolveUrl("~/") + '/Images/Icons/map_marker_01.png';
    var mapOptions = {
        // How zoomed in you want the map to start at (always required)
        zoom: 16,
        styles:[
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": 33
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2e5d4"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#c5dac6"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#c5c6c6"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e4d7c6"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#fbfaf7"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#acbcc9"
            }
        ]
    }
        ],
        streetViewControl: false,
        mapMaker: true,
        scrollwheel: true,
        //disableDefaultUI: true,
        //noClear:true,
        panControl: true,
        panControlOptions: {
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.LEFT_TOP
        },
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DEFAULT,
            position: google.maps.ControlPosition.TOP_LEFT
        },
        // The latitude and longitude to center the map (always required)
        center: myLatlng, // New York
        /*addressControl: true,
		addressControlOptions: {
		    position: google.maps.ControlPosition.BOTTOM_CENTER
		},*/
		// How you would like to style the map. 
        // This is where you would paste any style found on Snazzy Maps. 

	};
	/*var marker = new MarkerWithLabel({
	    position: new google.maps.LatLng(16.0612044, 108.2356365),
		map: map,
		icon: image,
		//labelContent: "Royal Hotel & Healthcare Resort Quy Nhon",
        labelAnchor: new google.maps.Point(0, 0),
        labelClass: "labels"
	});*/
	var marker2 = new MarkerWithLabel({
	    position: new google.maps.LatLng(16.0612044, 108.2356365),
	    map: map2,
	    icon: image,
	    //labelContent: "Royal Hotel & Healthcare Resort Quy Nhon",
	    labelAnchor: new google.maps.Point(0, 0),
	    labelClass: "labels"
	});
	// Get the HTML DOM element that will contain your map 
	// We are using a div with id="map" seen below in the <body>
	//var mapElement = document.getElementById('map_canvas');
	var mapElement2 = document.getElementById('map_contact_canvas');
	// Create the Google Map using out element and options defined above
	//var map = new google.maps.Map(mapElement, mapOptions);
	var map2 = new google.maps.Map(mapElement2, mapOptions);
	//marker.setMap(map);
	marker2.setMap(map2);
}