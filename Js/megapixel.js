﻿//function handleError() { return true; }
//window.onerror = handleError;
$(document).on('scroll', function () {
    //ShowLogo();
    if (currentBootstrapBreakPoint != 'lg') {
        checkScroll();
    }    
});
/*$(window).bind('mousemove', function() {
    FixedMenu();
});*/
var is_touch_device = false;
var mpt = 320;
var smh = 120;
var cvs = 3;
$(document).ready(function () {
    var url = window.location.href;
    /*if (url.indexOf("News.html") > 0 || url.indexOf("News/SID") > 0) {
        NiceScroll();
        GetWeather();
        $('.menu_bg').addClass("white_bg");
        $('.menu_item').removeClass('selected');
        $('.menu_item.menu_news').addClass('selected');
    } else {*/
        //pjax();
        //Scroll2ID();
        LoadingLine();
        dynamicWOW('home_wow');
        //dynamicWOW('links_wow');    
        //TextControlFocus('.news_letter_tf');
        //MapLocation();
        LanguageMenu();
        //ShowFancybox('.gallery_img');
        //ShowVideo('.video_link');
        UtilitiesSliderLoad($(".utilities_slider"));
        SwipeEffect();
        GetWeather();
        //RoadmapTabs();
        ShowFancybox(".view_28");
        //LazyImage();
        //is_touch_device = 'ontouchstart' in document.documentElement;*/
        if ("ontouchstart" in window || navigator.msMaxTouchPoints) {
            is_touch_device = true;
        } else {
            is_touch_device = false;
        }
        if (currentBootstrapBreakPoint === 'lg') {
            if (url.indexOf("/News") < 0) {
                Fullpage();
            }            
            NiceScroll();
            /*$('#map_canvas').height($(window).height() - 68);
            $('#map_canvas').width($('.about_right_bg').width() + 18);*/
            /*$('.project_left_bg').height($(window).height() - 68);*/
        } else if (currentBootstrapBreakPoint === 'md') {
            PageHeight();
            NiceScroll();
            TabletMenu();
            Scroll2ID();
            /*$('#map_canvas').height($(window).height() - smh);*/
        }
        else if (currentBootstrapBreakPoint === 'sm') {
            PageHeightSM();
            //NiceScroll();
            TabletMenu();
            MobileMenu();
            Scroll2ID();
            /*smh = 220;
            $('#map_canvas').height($(window).height() - smh);*/
            //cvs = 2;
        }
        else if (currentBootstrapBreakPoint === 'xs') {
            PageHeight();
            MobileMenu();
            TabletMenu();
            /*$('#map_canvas').height(1168);*/
            //cvs = 1;
        }
        /*PartnerCarousel(cvs);*/
        /*$('body img').load(function () {
            
        });*/
        VirtualTour();
        AboutUsTabs();
        ProjectTabs();
        //alert(ResolveUrl("~/"));
        /*$(".about_image_item").css({
            width:'100px'
        });
        $(".about_image_item").iviewer({
            src: ResolveUrl("~/uploads/Bg/") + "about_02.jpg"
        });*/
    /*}*/
    
});
/*function LazyImage() {
    $(".about_image, .project_image_bg img, .utilities_slider img, .news_item_image img").lazyload({
        effect: "fadeIn"
    });
}*/
function VirtualTour(){
    $('.virtual_tour_btn').stop().click(function () {
        //$('.body_bg').hide();
        //$('.virtual_tour_bg').fadeIn(1000);
        lang = $.cookie("HGTLanguage");
        $('.virtual_tour_bg iframe').attr("src", "http://vietnam360.tk/HiyoriGarden/?lang="+lang);
        $('.virtual_tour_bg').animate({ left: 0 }, 600);
    });
    $('.close_btn').stop().click(function () {
        //$('.body_bg').show();
        //$('.virtual_tour_bg').fadeOut(1000);
        $('.virtual_tour_bg').animate({ left: '100%' }, 600);
    });
    $(document).keyup(function (e) {
        if (e.keyCode === 27) $('.virtual_tour_bg').animate({ left: '100%' }, 600);
    });
}
function AboutUsTabs() {
    $('.about_menu_item').eq(0).addClass('active');
    $('.about_content_item').eq(0).show();
    $('.about_image_item').eq(0).show();
    $('.about_menu_item').not('.not_active').stop().click(function () {
        $('.about_menu_item').removeClass('active');
        //$('.about_content_item').removeClass('selected');
        $('.about_content_item').hide();
        $('.about_image_item').hide();
        ind = $(this).index('.about_menu_item');
        $('.about_content_item').eq(ind).fadeIn(500);
        $('.about_image_item').eq(ind).fadeIn(500);
        $(this).addClass('active');
        /*if (ind === 1) {
            try { loadMap(); } catch (ex) { }
        }*/
    });
}
function ProjectTabs() {
    //NivoSliderLoad("#floorplan_slider, #Type_A_slider, #Type_B_slider");
    NivoSliderLoad($(".nivoSlider").eq(0));
    $('.project_content_item').eq(0).show();
    $('.project_menu_item').eq(0).addClass('active');
    $('.project_image_item').eq(0).show(function(){});
    $('.project_menu_item').stop().click(function () {
        if ($('.project_menu_item').eq(0).hasClass('firstClick')) {
            $('.nivoSlider').each(function (index) {
                if(index>0) NivoSliderLoad($(".nivoSlider").eq(index));
            });
            $('.project_menu_item').removeClass('firstClick');
        }
        $('.project_menu_item').removeClass('active');
        //$('.project_content_item').removeClass('selected');
        $('.project_content_item').hide();
        $('.project_image_item').hide();
        ind = $(this).index('.project_menu_item');

        /*$('#floorplan_slider').data('nivoslider').stop();
        $('#Type_A_slider').data('nivoslider').stop();
        $('#Type_B_slider').data('nivoslider').stop();
        slider = $('.nivoSlider').eq(ind).attr("id");
        $("#" + slider).data('nivoslider').start();*/
        //if ()
        /*try {
            $('.nivoSlider').each(function (index) {
                $('.nivoSlider').eq(index).data('nivoslider').stop();
            });
        } catch (ex) {
            NivoSliderLoad($(".nivoSlider").eq(1));
            NivoSliderLoad($(".nivoSlider").eq(2));
            $('.nivoSlider').each(function (index) {
                $('.nivoSlider').eq(index).data('nivoslider').stop();
            });
            alert();
        }*/
        $('.nivoSlider').each(function (index) {
            $('.nivoSlider').eq(index).data('nivoslider').stop();
        });
        $('.nivoSlider').eq(ind).data('nivoslider').start();
        $('.project_content_item').eq(ind).fadeIn(500);
        //$('.project_image_item').eq(ind).fadeIn(1000);
        $('.project_image_item').eq(ind).show();
        $(this).addClass('active');
        if (ind === 1) {
            try { loadMap(); } catch (ex) { }
        }
    });
    /*$('.project_menu_item').one("click", function () {
        ind = $(this).index('.project_menu_item');
        //slider = $('.nivoSlider').eq(ind).attr("id");        
        NivoSliderLoad($(".nivoSlider").eq(ind));
    });*/
}
function LoadingLine() {
    $("body").queryLoader2({
        barColor: "#ffffff",
        backgroundColor: "#005559",
        percentage: false,
        barHeight: 2,
        completeAnimation: "grow",
        deepSearch: false,
        minimumTime: 1000,
        maxTime:6000,
        onLoadComplete: function() {$('#qLpercentage').fadeOut(200)}
    });
}
//-----------------------------------------------Roadmap Tabs----------------------------------------------//
/*function RoadmapTabs() {
    $('.roadmap_tab_item').eq(0).addClass('active');
    $('.tab_content_item_bg').eq(0).show();
    $('.roadmap_tab_item').stop().click(function () {
        $('.tab_content_item_bg').hide();
        $('.roadmap_tab_item').removeClass('active');
        $(this).addClass('active');
        $('.tab_content_item_bg').eq($(this).index('.roadmap_tab_item')).fadeIn(1000);
    });
}*/
function Fullpage() {
    $('.body_bg').fullpage({
        anchors: ['Home', 'AboutUs', 'ProjectInformation', 'Utilities', 'View', 'Video', 'Contact'],
		menu: '.main_menu',
        scrollingSpeed: 500,
        verticalCentered: false,
        easing: 'swing',
        easingcss3: 'linear',
        resize : true,
        /*navigation: true,
        navigationPosition: 'right',*/
        scrollBar: true,
        onLeave: function(index, nextIndex, direction) {
            //alert(nextIndex);
            $('.menu_item').removeClass('selected');
            if (nextIndex === 1) {
                dynamicWOW('home_wow');
                $('.menu_bg').removeClass("white_bg");
            }
            else if(nextIndex === 2){ dynamicWOW('about_wow'); } 
            else if(nextIndex === 3){ dynamicWOW('project_wow'); }
            else if (nextIndex === 4) { dynamicWOW('utilities_wow'); }
            else if (nextIndex === 5) { dynamicWOW('news_wow'); }
            else if (nextIndex === 6) { dynamicWOW('video_wow'); }
            else if (nextIndex === 7) { dynamicWOW('contact_wow'); }
            $('.menu_item').eq(nextIndex - 1).addClass('selected');
            if (nextIndex > 1) {
                $('.menu_bg').addClass("white_bg");
            }
        }        
	});
}
function PageHeight() {
    var ph1 = 9 / 16 * $(window).width();
    var ph2 = 10 / 16 * $(window).width();
    var ph3 = (7.8 / 16 * $(window).width())+50;
    $('.home_page_bg').height($(window).height());
    $('.overview_page_bg').height(ph1);
    $('.location_page_bg').height(ph2);
    $('.links_page_bg').height(ph3);
    $('.facilities_page_bg').height(ph2);
    $('.floorplan_page_bg').height(ph1);
    $('.villas_page_bg').height(ph2);
    $('.roadmap_page_bg').height(ph2+38);
    $('.gallery_page_bg').height(ph1-50);
    $('.contact_page_bg').height(ph2+38);
    $('.roadmap_tab_item_img div').height($('.roadmap_tab_item_content_bg').height());   
    //dynamicWOW('location_wow'); 
    AllImageLoaded();
}
function PageHeightSM() {
    var ph1 = 9 / 16 * $(window).width();
    //var ph2 = 10 / 16 * $(window).width();
    //var ph3 = (7.8 / 16 * $(window).width())+50;
    $('.home_page_bg').height($(window).height());
    $('.overview_page_bg').height(ph1);
    $('.floorplan_page_bg').height(ph1);
    $('.villas_page_bg').height(ph1+168);
    $('.roadmap_page_bg').height(ph1+20);
    $('.gallery_page_bg').height(ph1-20);
    /*$('.location_page_bg').height(ph2);
    $('.links_page_bg').height(ph3);
    $('.facilities_page_bg').height(ph2);
    $('.floorplan_page_bg').height(ph1);
    $('.villas_page_bg').height(ph1);
    $('.roadmap_page_bg').height(ph2+38);
    $('.gallery_page_bg').height(ph1-50);
    $('.contact_page_bg').height(ph2+38);
    $('.roadmap_tab_item_img div').height($('.roadmap_tab_item_content_bg').height());*/   
    //dynamicWOW('location_wow'); 
    AllImageLoaded();
}
function TabletMenu() {
    $('section.section').each(function() {
        $(this).attr('id', $(this).attr('id').replace("_Page", ""));
    }).promise().done(function(){ 
        Scroll2ID();     
    });
    
}
function dynamicWOW(boxClass) {
    wow = new WOW({
        boxClass: boxClass
    });
    wow.init();
    //console.log(boxClass);
}
var home_height = 0;
//var about_height = 0;
var overview_height = 0;
var location_height = 0;
var links_height = 0;
var facilities_height = 0;
var floorplan_height = 0;
var villas_height = 0;
var roadmap_height = 0;
//var reason_height = 0;
var gallery_height = 0;
//var contact_height = 0;
var wrh = 32;
function AllImageLoaded() {    
    home_height = $('.home_page_bg').height();
    //about_height = home_height + $('.about_page_bg').height() + $('.overview_page_bg').height();
    overview_height = home_height + $('.overview_page_bg').height();   
    location_height = overview_height + $('.location_page_bg').height();
    links_height = location_height + $('.links_page_bg').height();
    facilities_height = links_height + $('.facilities_page_bg').height();
    floorplan_height = facilities_height + $('.floorplan_page_bg').height();
    villas_height = floorplan_height + $('.villas_page_bg').height();
    roadmap_height = villas_height + $('.roadmap_page_bg').height();
    //reason_height = floorplan_height + $('.reason_page_bg').height();
    gallery_height = roadmap_height + $('.gallery_page_bg').height() + $('.contact_page_bg').height();
    //contact_height = gallery_height + $('.contact_page_bg').height();
}
function checkScroll() {
    
    if ($(document).scrollTop() > home_height-82) {
        $('.menu_bg').addClass("white_bg");
    } else {
        $('.menu_bg').removeClass("white_bg");
    }
    if (100 < $(document).scrollTop() && $(document).scrollTop() < 100+wrh) {
        dynamicWOW('overview_wow');
    }
    else if (home_height < $(document).scrollTop() && $(document).scrollTop() < home_height + wrh) {
         dynamicWOW('home_wow');
         dynamicWOW('location_wow');
    } 
    else if (overview_height < $(document).scrollTop() && $(document).scrollTop() < overview_height + wrh) {
         dynamicWOW('overview_wow');
         dynamicWOW('links_wow');
    } 
    /*else if (overview_height < $(document).scrollTop() && $(document).scrollTop() < overview_height + wrh) {
        dynamicWOW('overview_wow');
        dynamicWOW('facilities_wow');
    }*/ 
    else if (location_height < $(document).scrollTop() && $(document).scrollTop() < location_height + wrh) {
        dynamicWOW('location_wow');
        dynamicWOW('facilities_wow');
    } 
    else if (links_height < $(document).scrollTop() && $(document).scrollTop() < links_height + wrh) {
        dynamicWOW('links_wow');
        dynamicWOW('floorplan_wow');
    }
    else if (facilities_height < $(document).scrollTop() && $(document).scrollTop() < facilities_height + wrh) {
        dynamicWOW('facilities_wow');
        dynamicWOW('villas_wow');
    } 
    else if (floorplan_height < $(document).scrollTop() && $(document).scrollTop() < floorplan_height + wrh)
    {
         dynamicWOW('floorplan_wow');
         dynamicWOW('roadmap_wow');
    } 
    else if (villas_height < $(document).scrollTop() && $(document).scrollTop() < villas_height + wrh)
    {
         dynamicWOW('villas_wow');
         dynamicWOW('gallery_wow');
         //dynamicWOW('contact_wow');
    }
    else if (roadmap_height < $(document).scrollTop() && $(document).scrollTop() < roadmap_height + wrh)
    {
         dynamicWOW('roadmap_wow');
         dynamicWOW('contact_wow');
    } 
    else if (gallery_height < $(document).scrollTop() && $(document).scrollTop() < gallery_height + wrh)
    {
         dynamicWOW('gallery_wow');
         dynamicWOW('contact_wow');
    } 
    /*else if (contact_height < $(document).scrollTop() && $(document).scrollTop() < contact_height + wrh)
    {
         dynamicWOW('contact_wow');
    }*/
}
function Scroll2ID() {
    $(".menu_item").mPageScroll2id({
        layout: "vertical",
        scrollSpeed: 900,
        autoScrollSpeed: true,
        scrollingEasing: "easeInOutCirc",
        pageEndSmoothScroll: true,
        offset:70,
        //clickEvents:false,
        clickedClass: "selected",
        highlightSelector:".menu_item",
        onComplete:function() {
            console.log("ok");
        },
        highlightClass:"selected",
        forceSingleHighlight:true
    });
    $('.header_logo').click(function() {
        $('body').animate({ scrollTop: 0}, 300);
    });
}
function ShowLogo() {
    try {
        if (currentBootstrapBreakPoint === 'xs' || currentBootstrapBreakPoint === 'sm') {
        }else{
            if ($(document).scrollTop() > mpt) {
                $(".header_logo").css({left: 0});
                $("header .lang_menu_bg").css({right: '24px'});
            }
            if ($(document).scrollTop() < mpt) {
                $(".header_logo").css({left: '-100%'});
                $("header .lang_menu_bg").css({right: '-100%'});
            }
        }
    } catch (ex) { }
}
/*function dynamicWOW(boxClass) {
    wow = new WOW({
        boxClass: boxClass,
        offset:50
    });
    wow.init();
}*/
/*function HomeSlideshowTitle() {
    $('#kb').bind('slid.bs.carousel', function (e) {
        $('.slideshow_title').html("").append($('.item.active .carousel_title').clone());
    });
    
}*/
function MapLocation() {
    $('.viewmap_btn').stop().click(function() {
        if ($(this).hasClass('active')) {
            $('.map_overlay').fadeIn();
            $(this).removeClass('active');            
        } else {
            $('.map_overlay').fadeOut();
            $(this).addClass('active');
        }
    });
}
function PartnerCarousel(cvs) {    
	$('.partner_bg.carousel').jCarouselLite({
		circular: true,
		mouseWheel: true,
		auto: true,
  		timeout: 3000,
		swipe:true,
		speed: 800,
		autoWidth: true,
		visible:cvs,
		responsive:true
	});
}
//-----------------------------------------------Carousel----------------------------------------------//
function CarouselControl() {
    try {
        if (currentBootstrapBreakPoint != 'xs' && currentBootstrapBreakPoint != 'sm') {
            $('.home_service_carousel').carousel({
                interval: 3800,
                //pause:false
            });
            $('.fdi-Carousel .item').each(function() {
                var next = $(this).next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }
                next.children(':first-child').clone().appendTo($(this));

                if (next.next().length > 0) {
                    next.next().children(':first-child').clone().appendTo($(this));
                } else {
                    $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
                }
            });
        }
    }catch(ex) {}
}
function SwipeEffect() {
    
    if (currentBootstrapBreakPoint === 'xs' || currentBootstrapBreakPoint === 'sm') {
        //$('.home_service').removeClass('carousel-inner');
    } else {
        $(".project_image_item, .utilities_page").swipe({
            excludedElements: "button, input, select, textarea, .noSwipe",
		    swipeLeft: function () {
		        $(this).find('a.nivo-nextNav').trigger('click');
		    },
		    swipeRight: function() {
		        $(this).find('a.nivo-prevNav').trigger('click');
		    },
		    //Default is 75px, set to 0 for demo so any distance triggers swipe
		    threshold:50
	    });
    }
    /*$('#kb').carousel({
        pause: false
    });*/
}
//----------------------------------------------Main Menu----------------------------------------------//
function MobileMenu(){
	$('.menu_btn').stop().click(function(){
		if($(this).hasClass("show")) {
		    HideMobileMenu();
		}
		else {
		    ShowMobileMenu();
		}
	});
    $('.main_menu a').stop().click(function(){
        HideMobileMenu();
    });
    $('.body_bg').stop().click(function(){
        HideMobileMenu();
    });
}

function ShowMobileMenu() {
    //$('.home_logo_bg').fadeOut();
	$('.body_bg').animate({opacity:'0.5'},300);
	$('.body_bg').addClass('body_blur');
	$('nav').animate({right:0},300);		    
	$('.menu_btn').addClass('show');
}
function HideMobileMenu() {
    $('nav').animate({ right: "-200px" }, 300, function () {
	    //$('.home_logo_bg').fadeIn();
	    $('.body_bg').animate({opacity:'1'},300);
	    $('.body_bg').removeClass('body_blur');
    });		    
    $('.menu_btn').removeClass('show');
}
function OnTouchDevice(control) {
    if (is_touch_device) {
        $(control).addClass('onTouchDevice');
    }
}
function BootstrapCarousel(control) {
    $(control).carousel({
        interval: 3000
    });
}
function ScrollToItem(selector){
	//$(selector).scrollTo(300);
	 $('body').animate({ scrollTop: parseInt($(selector).offset().top-80)}, 300);
}
//-----------------------------------------------Sub Menu----------------------------------------------//
function SubMenu() {    
    //$('.sub_menu_bg').css({width:$(window).width()+'px', left:'-220px', right:'0'});
    $('.parent_menu_item').stop().hover(
        function () {                        
            $(this).find('.sub_menu_bg').stop().slideDown();
        },
        function () {
            $(this).find('.sub_menu_bg').stop().slideUp();
        }
    );
}
function NiceScroll(){
	if (currentBootstrapBreakPoint === 'lg' && is_touch_device === false) {
		$("html").niceScroll({
			cursorborder: "none",
			cursorborderradius: "0",
			cursorwidth: "3px",
			cursorcolor: "#005559",
			cursoropacitymin: 0.8,
			cursoropacitymax: 1,
			background: "rgba(255,255,255,0.2)",
			//scrollspeed: 60,
			//mousescrollstep: 80,
			enabletranslate3d:false,
			smoothscroll:false,			
			autohidemode: true,
			zindex: "9999999"	
		});
	}
}
//-----------------------------------------------Set Current Menu Item----------------------------------------------//
function SetCurrentMenuItem(control) {
    try {
        var url = window.location.pathname;
        $(control).removeClass('selected'); 
        /*if (url.indexOf("Home-page") > 0 || url.indexOf(".html") < 0) {
            $(control).eq(0).addClass('selected');
        } else {*/
            url = url.replace(ResolveUrl('~/'), "");
            if (url.indexOf("/") > -1) {
                url = url.substring(0, url.indexOf("/"));                
            }
            url = url.replace("-","").replace(".html","").replace("Details","");
            //alert(url);
            $(control).each(function() {
                var href = $(this).attr("href");
                href = href.replace(ResolveUrl('~/'), "");
                /*if (href.indexOf("/") > -1) {
                    href = href.substring(0, href.indexOf("/"));                
                }*/
                href = href.replace("-","").replace(".html","").replace("Details","");
                //alert(href);
                if (href == url) {
                    $(this).addClass('selected');
                    return false;
                }         
            });
        //} 
    }catch(ex){}
            
}
function SetCurrentMobileMenuItem(control) {
    try {
        var url = window.location.href;
        $(control).removeClass('selected'); 
        if (url.indexOf("#!Home/") > 0 || url.indexOf(".mpx") < 0) {
            $(control).eq(0).addClass('selected');
        } else {
            url = url.substring(url.indexOf("#!"));
            url = url.substring(0, url.indexOf("/"));
            $(control).each(function() {
                var href = $(this).attr("href");
                href = href.substring(href.indexOf("#!"));
                href = href.substring(0, href.indexOf("/"));
                if (href == url) {
                    $(this).addClass('selected');
                }         
            });
        } 
    }catch(ex){}
            
}
function LanguageMenu(){
   $('.lang_menu_bg .en').click(function () {
        $.cookie("HGTLanguage", "en", { expires: 7, path: '/' });
        location.reload();
    });
    $('.lang_menu_bg .vi').click(function () {
        $.cookie("HGTLanguage", "vi", { expires: 7, path: '/' });
        location.reload();
    }); 
    $('.lang_menu_bg .jp').click(function () {
        $.cookie("HGTLanguage", "ja-JP", { expires: 7, path: '/' });
        location.reload();
    });
}

/*function BlurGlass(){
	$('.home_room_content_bg').blurjs({
		source: '.home_room_bg',
		overlay: 'rgba(255,255,255,0.66)',
		radius:10
	});
}*/
//----------------------------------------------Get Weather Ajax--------------------------------------------//
function GetWeather(){
	try
	{
		$.simpleWeather({
			zipcode: '',
			woeid: '20070085',
			location: '',
			unit: 'c',
			success: function(weather) {
				var f = Math.round(weather.temp * 9 / 5 + 32);
				$('.weather_widget_temperature').html("<span class='icon-"+ weather.code + " weather_icon'></span>" + f + '&deg;F/' + weather.temp + '&deg;C');
			},
			error: function(error) {
			  //alert('error');
			}
	  });
	}catch(ex){}	
	setTimeout("SetLocalTime()", 5);
	setInterval("SetLocalTime()", 30000);
}
//-----------------------------------------------Set Local Time----------------------------------------------//
function SetLocalTime(){
	var tz = new TimeZoneDB;
	tz.getJSON({
		key: "DWXJXQY6XSXT",
		zone: "Asia/Ho_Chi_Minh"
	}, function(data){
		var myDate = new Date(data.timestamp*1000);
		var hour = myDate.getUTCHours();
		var minutes = myDate.getUTCMinutes();
		var tt = "AM";
		if(hour > 12){
			hour = hour -12;
			tt = "PM";	 
		}
	    if(hour<10) hour = "0" + hour;
	    if(minutes<10) minutes = "0" + minutes;
		var time = hour + ":" + minutes + " " + tt;
		$('.weather_widget_time').html(time);
	    //$('.weather_widget').show();
	});	
}
//----------------------------------------------Google Map----------------------------------------------//
function GoogleMap(control){
	try{
	    if (currentBootstrapBreakPoint === 'xs' || currentBootstrapBreakPoint === 'sm') {
	        //$(control).attr("href", "https://www.google.com/maps/place/Cat+Ba+Sunrise+Resort/@20.715477,107.050363,10z/data=!4m2!3m1!1s0x0:0x60c46fbcc9f9aae3?hl=en-US");
	        $(control).attr("target", "_blank");
	    } else {
	        $(control).fancybox({
	            width: '900',
	            height: '500',
	            autoScale: true,
	            openEffect: 'elastic',
	            closeEffect: 'elastic',
	            scrolling: 'no',
	            fitToView: true,
	            type: 'iframe',
	            helpers: {
	                title: {
	                    type: 'over'
	                }
	            }
	        });
	    }
	}catch(ex){}
}
//-----------------------------------------------Nivo Slider----------------------------------------------//

function NivoSliderLoad(control) {
    control.nivoSlider({
        effect: 'sliceDown', // Specify sets like: 'fold,fade,sliceDown'
        slices: 30, // For slice animations
        boxCols: 20, // For box animations
        boxRows: 10, // For box animations
        animSpeed: 500, // Slide transition speed
        pauseTime: 5000, // How long each slide will show
        startSlide: 0, // Set starting Slide (0 index)
        directionNav: true, // Next & Prev navigation
        directionNavHide: false, // Only show on hover
        controlNav: true, // 1,2,3... navigation
        controlNavThumbs: false, // Use thumbnails for Control Nav
        pauseOnHover: false, // Stop animation while hovering
        manualAdvance: false, // Force manual transitions
        prevText: 'Prev', // Prev directionNav text
        nextText: 'Next', // Next directionNav text
        randomStart: false, // Start on a random slide
        manualCaption: true
        //captionOpacity: 0//,
        
			//afterLoad: function () { $('#slider .nivo-caption').css({left:0, opacity:'1'}) },
			//beforeChange: function () { $('#slider .nivo-caption').css({left:'-100%', opacity:'0'}) },
			//afterChange: function () { $('#slider .nivo-caption').css({left:0, opacity:'1'}) }
//			slideshowEnd: function(){}, // Triggers after all slides have been shown
//			lastSlide: function(){}, // Triggers when last slide is shown
//			afterLoad: function(){Cufon.refresh();} // Triggers when slider has loaded
    });
    /*if (currentBootstrapBreakPoint === 'lg') {
        SliderPosition();
    }*/
}
function UtilitiesSliderLoad(control) {
    control.nivoSlider({
        effect: 'sliceDown', // Specify sets like: 'fold,fade,sliceDown'
        slices: 30, // For slice animations
        boxCols: 20, // For box animations
        boxRows: 10, // For box animations
        animSpeed: 300, // Slide transition speed
        pauseTime: 8000, // How long each slide will show
        startSlide: 0, // Set starting Slide (0 index)
        directionNav: true, // Next & Prev navigation
        directionNavHide: false, // Only show on hover
        controlNav: true, // 1,2,3... navigation
        controlNavThumbs: false, // Use thumbnails for Control Nav
        pauseOnHover: false, // Stop animation while hovering
        manualAdvance: false, // Force manual transitions
        prevText: 'Prev', // Prev directionNav text
        nextText: 'Next', // Next directionNav text
        randomStart: false, // Start on a random slide        
        captionOpacity: 1,
        afterLoad: function () { $('#utilities_slider .nivo-caption').css({ left: 0, opacity: '1' }) },
        beforeChange: function () { $('#utilities_slider .nivo-caption').css({ left: '-550px', opacity: '0' }) },
        afterChange: function () { $('#utilities_slider .nivo-caption').css({ left: 0, opacity: '1' }) }
        //			slideshowEnd: function(){}, // Triggers after all slides have been shown
        //			lastSlide: function(){}, // Triggers when last slide is shown
        //			afterLoad: function(){Cufon.refresh();} // Triggers when slider has loaded
    });
    /*if (currentBootstrapBreakPoint === 'lg') {
        SliderPosition();
    }*/
}
function SliderPosition() {
    $('.nivoSlider img').load(function() {
        $('.nivoSlider').css({
            bottom: ((($('.nivoSlider').height() - $(window).height())/2)) + 'px'
        });
    });    
}
//function SliderNav(){
//	var cnw = $('.theme-default .nivo-controlNav').width();
//	//alert(cnw);
//	$('.theme-default .nivo-controlNav').css({marginLeft:-cnw/2+'px'});
//	$('.theme-default a.nivo-nextNav').css({marginRight:445-cnw/2+'px'});
//	$('.theme-default a.nivo-prevNav').css({marginLeft:445-cnw/2+'px'});
//}
//-----------------------------------------------Share This Hover-----------------------------------------//
//function ShareThisHover(control) {
//	$(control).hover(
//		  function () {
//			  //$('.sharethis_bg').stop().fadeTo(500, 1);	
//			  //$('.sharethis_bg').stop().show(500);
//			  $('.sharethis_bg').stop().animate({height:'68px'},300);	  
//		  }, 
//		  function () {
//			  //$('.sharethis_bg').stop().fadeTo(500, 0);
//			  //$('.sharethis_bg').stop().hide(500);
//			  $('.sharethis_bg').stop().animate({height:'0px'},300);	
//		  }	
//		  
//	);
//	$('.sharethis_bg').hover(
//		  function () {
//			  //$(this).stop().fadeTo(0, 1);
//			  //$(this).stop().show(500);
//			  $(this).stop().animate({height:'68px'},300);		  
//		  }, 
//		  function () {
//			  //$(this).stop().fadeTo(500, 0);
//			  //$(this).stop().hide(500);
//			  $(this).stop().animate({height:'0px'},300);
//		  }	
//		  
//	);
//}
////-----------------------------------------------Check Room----------------------------------------//
//function CheckRoom(){
//	var dates = $(".tf_check_in, .tf_check_out").datepicker({
//            minDate: 0,
//            dateFormat: 'dd/mm/yy',
//            onSelect: function(selectedDate) {
//                var option = $(this).hasClass("tf_check_in") ? "minDate" : "maxDate",
//				instance = $(this).data("datepicker"),
//				date = $.datepicker.parseDate(
//				instance.settings.dateFormat ||
//				$.datepicker._defaults.dateFormat,
//				selectedDate, instance.settings);
//				dates.not(this).datepicker("option", option, date);
//            }
//    });
//	$('.check_in_btn').click(function() {
//		$(".tf_check_in").datepicker("show");
//    });
//	$('.check_out_btn').click(function() {
//		$(".tf_check_out").datepicker("show");
//    });
//}
//----------------------------------------------Dialog Message--------------------------------------------//
function DialogMessage() {
    $("#dialog-message").dialog({
        bgiframe: true,
        autoOpen: false,
        resizable: false,
        minheight: 220,
        //minWidth: 500,
        width: 'auto',
        maxWidth: '100%',
        modal: true,
        closeOnEscape: true,
        buttons: {
            Ok: function () {
                $(this).dialog("close");
            }
        }
    });
    $('#dialog-message').dialog({ dialogClass: "fixed_dialog" });
    $('.fixed_dialog.ui-dialog').css({ position: "fixed" });
}
function QueryString(url, str) {
    var val = url.substring(url.indexOf(str) + str.length);
    val = val.substring(0, val.indexOf("/"));
    return val;
}
//-----------------------------------------------View Full Image-----------------------------------------//
function ViewFullImage(control){
	$(control).attr("href", function() {
         return $(this).children('img').attr('src');
    });
	//$(control).attr("title", function() {
//         return $(this).children('.img_name').text();
//    });
	$(control).attr("rel", "full_images");
	ShowFancybox(control);
}
//----------------------Auto Fancy Box------------------------------//
function AutoFancyBox(control){
	for(i=0; i<$(control).length;i++){
		$(control).eq(i).parent('a').attr("href", $(control).eq(i).attr('src'));
		$(control).eq(i).parent('a').attr("rel", 'about_img');
	}
	//$(control).attr("rel", 'about_img');
}
//-----------------------------------------------Open Datepicker-----------------------------------------//
function OpenDatepickerButton(control, datepicker){
    $(control).click(function() {
		var visible = $('#ui-datepicker-div').is(':visible');
		$(datepicker).datepicker(visible ? 'hide' : 'show');
    });
}
function OpenDatepicker(datepicker){
	$(datepicker).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd/mm/yy',
		autoSize: true,
		showAnim: 'slideDown'
	});
}
//----------------------------------------------Get Query String---------------------------------------------//
function getQueryString(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search.substring(1));
    if (results == null) return "";
    else return results[1];
}
//----------------------------------------------Check URL String---------------------------------------------//
function CheckURLString(checkStr) {
    //var urlStr = window.location
    if (window.location.href.indexOf(checkStr) != -1) {
        return true;
    }
    else return false;
}
//----------------------Grayscale Image Effect------------------------------//
function GrayscaleImageEffect(selector, imgHeight){
	grayImg = $(selector + ' img').attr('src');
	$(selector).css({background: 'url('+grayImg+') no-repeat'});
	$(selector + ' img').css({marginTop: '-'+imgHeight+'px', display:'none'});	
	$(selector).hover(
	  function () {
			$(this).children('img').stop().fadeTo(700, 1);
	  }, 
	  function () {
			$(this).children('img').stop().fadeTo(500, 0);
	  }
	);
}
//-----------------------------------------------Combobox----------------------------------------//
function Combobox(control){
	$(control).selectbox();
}
//-----------------------------------------------CSS 3 for IE-----------------------------------------//
function Css3ForIE(control){
	if (window.PIE) {
        $(control).each(function() {
            PIE.attach(this);
        });
	}
}
//-----------------------------------------------To Top Page-----------------------------------------//
function ToTopPage(control){
    $(document).scroll(function () {
        if ($(window).scrollTop() != 0) {
            $(control).fadeIn();
        } else {
            $(control).fadeOut();
        }
    });
    $(control).click(function () {
        $('html, body').animate({ scrollTop: 0 }, 500);
        return false;
    });
}
//----------------------Show Fancybox------------------------------//
function ShowFancybox(control){
	$(control).fancybox({
		//width				: '50%',
	    //height				: '75%',
	    maxWidth: '92%',
		autoScale		: true,				
		//openEffect 	:	'elastic',
		//closeEffect 	:	'elastic',
		fitToView	: true,
		scrolling: 'no',
		//type: 'image',
		helpers: {
            title: {
                type: 'over'
            },
            thumbs: {
                width: 76,
                height: 56,
                position: 'bottom'
            }
        },
        beforeLoad: function () {            
            if (this.title == "") {
                if ($(this.element).attr('caption') == "") {
                    this.title = "Marina Complex Da Nang City";
                } else {
                    this.title = $(this.element).attr('caption');
                }
            }
            
        }
		//type			: 'iframe',
		//titlePosition	: 'over'
		//titleFormat		: function(title, currentArray, currentIndex, currentOpts) { return '<span id="fancybox-title-over">' + (title.length ? title : '') + '<br />Image ' + (currentIndex + 1) + ' / ' + currentArray.length + '</span>';}
	});
}
function ShowVideo(control) {
    $(control).fancybox({
        width				: '60%',
		height				: '75%',
		fitToView	: false,
		maxWidth		: '78%',
		maxHeight		: '86%',
		autoSize	: true,
		closeClick	: false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		type: 'iframe',
		helpers: {
            title: {
                type: 'inside'
            },
            thumbs: {
                width: 70,
                height: 50,
                position: 'bottom'
            }
        },
        beforeLoad: function () {            
            if (this.title == "") {
                if ($(this.element).attr('caption') == "") {
                    this.title = "Marina Complex Da Nang City";
                } else {
                    this.title = $(this.element).attr('caption');
                }
            }
            
        }		
	});
}
function ShowPanorama(control){
    if (currentBootstrapBreakPoint === 'xs' || currentBootstrapBreakPoint === 'sm' ) {
        $(control).attr("target", "_blank");
    } else {
         $(control).fancybox({
            width: '75%',
            height: '95%',
            autoScale: true,
            //openEffect: 'elastic',
            //closeEffect: 'elastic',
            scrolling: 'no',
            fitToView: false,
            type: 'iframe',
            beforeLoad: function() {
                if (this.title == "") {
                    this.title = "Panorama 360";
                }
            },
            helpers: {
                title: {
                    type: 'over'
                },
                thumbs: {
                    width: 70,
                    height: 50,
                    position: 'bottom'
                }
            }
        });
    }
}
//----------------------------------------------Text Control Focus----------------------------------------------
function TextControlFocus(control) {
    var search_label = $(control).parent().find('label');
    $(control).on('input', function () {
        if ($(this).val() === "") {
            search_label.show();
        } else {
            search_label.hide();
        }
    });
}
function PageContentImages() {
    /*if (CheckURLString("/localhost/")) {
        $('.page_bg img').off().each(function() {
            if ($(this).attr('src').indexOf('uploads/') > -1 && $(this).attr('src').indexOf('/SunSpa/') < 0) {
                $(this).attr('src', $(this).attr('src').replace('uploads/', ResolveUrl('~/uploads/')));
            }
        });
    } else {
        $('.page_bg img').off().each(function() {
            if ($(this).attr('src').indexOf('uploads/') > -1) {
                $(this).attr('src', $(this).attr('src').replace('uploads/', ResolveUrl('~/uploads/')));
            }
        });
    }*/
    $('.page_bg img').on('error', function() {
        $(this).unbind('error').attr('src', $(this).attr('src').replace('uploads/', ResolveUrl('~/uploads/')));
        ViewFullImage($(this).parent('a'));
    });
}
//-----------------------------------------------Resolve Url-----------------------------------------//
function ResolveUrl(url) {
    if (url.indexOf("~/") == 0) {
        url = baseUrl + url.substring(2);
    }
    return url;
}
// Replaces all instances of the given substring.
String.prototype.replaceAll = function (
strTarget, // The substring you want to replace
strSubString // The string you want to replace in.
) {
    var strText = this;
    var intIndexOfMatch = strText.indexOf(strTarget);

    // Keep looping while an instance of the target string
    // still exists in the string.
    while (intIndexOfMatch != -1) {
        // Relace out the current instance.
        strText = strText.replace(strTarget, strSubString)

        // Get the index of any next matching substring.
        intIndexOfMatch = strText.indexOf(strTarget);
    }

    // Return the updated string with ALL the target strings
    // replaced out with the new substring.
    return (strText);
}
function Tooltip(){
	$("[title]").style_my_tooltips({ 
		tip_follows_cursor:false,
		tip_delay_time:700,
		tip_fade_speed:300,
		attribute:"title"
	});	
}
//-----------------------------------------------Gridster Gallery----------------------------------------------//
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}    
function GridsterGallery() {
    gridster = $(".gridster ul").gridster({
         widget_margins: [8, 8],
            widget_base_dimensions: [106, 106],
          helper: 'clone',
          resize: {
            enabled: true
          }
        }).data('gridster');
//    gridster.resize_widget(gridster.$widgets.eq(getRandomInt(0, 18)), getRandomInt(1, 2), getRandomInt(1, 2));
//    gridster.serialize();
}
//function ShowGallery(control) {
//    $(control).fancybox({
//        //maxWidth: '85%',
//        //maxHeight: '85%',
//        loop: true,
//        prevEffect: 'elastic',
//        nextEffect: 'elastic',
//        //prevSpeed: 'slow',
//        //nextSpeed: 'slow', 
//        nextEasing: 'linear',
//        prevEasing: 'linear',
//        //        openEffect	: 'none',
//        //		closeEffect	: 'none',
//        //autoSize: true,
//        helpers: {
//            title: {
//                type: 'inside'
//            },
//            thumbs: {
//                width: 70,
//                height: 50,
//                position: 'bottom'
//            }
//        },
//        beforeLoad: function () {
//            this.title = $(this.element).attr('caption');
//            if (this.title == "") {
//		        this.title = "Catba Sunrise Resort";
//		    }
//        }
////        beforeLoad: function () {
////            this.title = $(this.element).attr('caption');
////        }
//        //type			: 'iframe',
//        //titlePosition	: 'over'
//        //titleFormat		: function(title, currentArray, currentIndex, currentOpts) { return '<span id="fancybox-title-over">' + (title.length ? title : '') + '<br />Image ' + (currentIndex + 1) + ' / ' + currentArray.length + '</span>';}
//    });
//}
function EqualHeight(control)
{
    if (currentBootstrapBreakPoint != 'xs') {
        var h = 0;
        //var h = "316px";
        $(control).each(function() {
            $(this).css({ 'height': 'auto' });
            if ($(this).outerHeight() > h) {
                h = $(this).outerHeight();
            }
            $(control).css({ 'height': h });
        });

    }
}